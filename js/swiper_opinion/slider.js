"use strict"


const swiper = new Swiper('.swiper-opinion', {
    // Optional parameters
    direction: 'horizontal',  
    loop: true,
    autoHeight: true,


    // If we need pagination
    pagination: {
      clickable: true,
      el: '.swiper-opinion__pagination',
    },
});




